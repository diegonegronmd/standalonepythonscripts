import arcpy
import os

#This script replaces workspaces for a set of MXDs.
#Up to two workspaces are supported.
#Inputs:
#folder = String Path of MXDs
#oldWS1 = String Path of first old WS uses findAndReplaceWorkspacePaths
#oldWS2 = String Path of second old WS uses replaceWorkspaces
#newWS = String Path of new WS

#Outpus:
#None. Changes are made and saved directly to the input file. 

folder = r'C:\Users\Diego Negron\Desktop\MXDs'

oldWS1 =  r'Z:\Hunter\Planning\AF_Adaptation_310_PGDB_20150312\AF_Adaptation_310_PGDB_20150312.mdb'
oldWS2 = r'Z:\Air Force GIS Data\ACC\Seymour Johnson AFB\seymour_johnson_afb_02Jun2015_wgs84_18n_cip_layers.gdb'
newWS = r'Z:\Air Force GIS Data\AF_Adaptation_310_PGDB_20150312.mdb'
outPutLocation = r'C:\Users\Diego Negron\Desktop\MXDs'

fileList = os.listdir(folder)
for listItem in fileList:
    if listItem.endswith(".mxd"):
        print "Trying with " + listItem
        mxd = arcpy.mapping.MapDocument(folder + "\\" + listItem)
        mxd.findAndReplaceWorkspacePaths(oldWS1,newWS, True)
        print "WS Replace 1 successfull"
        mxd.replaceWorkspaces(oldWS2,"FILEGDB_WORKSPACE", newWS,"ACCESS_WORKSPACE", False)
        print "WS Replace 2 successfull"
        mxd.save()
        del mxd
    
