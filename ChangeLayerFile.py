import arcpy
import os

#This script applies a set of Esri .lyr files to layers in a set of .MXDs.
#The layer files have to match the names in the TOC of the map
#This script has not been optimized to cycle through multiple data frames.
#Inputs:
#inputMapFolder - Folder with MXDs
#layerFilesFolder - Folder with layer Files
#Output:
#None. Changes are made and saved directly to the input file

inputMapFolder = r'C:\Users\Diego Negron\Desktop\Delivered MXDs_HSIP_update(V3)'
layerFilesFolder = r'C:\Users\Diego Negron\Desktop\SDSFIE'


fileList = os.listdir(inputMapFolder)
lyrFileList = os.listdir(layerFiles)
lyrFileNameList = []
for lyr in lyrFileList:
    lyrFileNameList.append(lyr.rstrip(".lyr"))

for listItem in fileList:
    if listItem.endswith(".mxd"):
        print "Trying with " + listItem
        mxd = arcpy.mapping.MapDocument(folder + "\\" + listItem)
        df = arcpy.mapping.ListDataFrames(mxd)[0]
       
        layerList = arcpy.mapping.ListLayers(mxd)
        #print layerList #used for debugging
        for layer in layerList:
            #print str(layer.name) #used for debugging
            if str(layer.name) in  lyrFileNameList:
                layerFileString = r'C:\Users\Diego Negron\Desktop\SDSFIE' + "\\" + layer.name + ".lyr"

                #Below print can be used for error checking after running.
                #I should really put this into a log file
                print layer.name + ", " + layerFileString + ", " + listItem.replace(".mxd","") 
                newLayer = arcpy.mapping.Layer(layerFileString)

                arcpy.mapping.UpdateLayer(df, layer, newLayer, True)
        mxd.save()
        del mxd
    
